# dirtree

> Simple directory tree project built with VueJS and deployed with Google Firebase

## Development

- Copy `.env.development` file to `.env` and update with appropriate values
- Install NPM dependencies
- Start the development server

```sh
# Install dependencies
npm install

# Compile for development (hot-reloads)
npm run start

# Compile for production (minifies)
npm run build

# Run unit tests
npm run test

# Lint code
npm run lint
```

## Deployment

Read the [Deployment](DEPLOYMENT.md) guide to deploy the app to [Google Firebase](https://firebase.google.com).

## Miscellaneous

- Icon generated with [Expo Icon Builder](https://buildicon.netlify.app/)
- PWA assets generated with [`vue-pwa-asset-generator`](https://www.npmjs.com/package/vue-pwa-asset-generator)
  - `npx vue-pwa-asset-generator -a src/assets/icons/logo.png -o public/img/icons`
