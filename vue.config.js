const path = require("path");

const src = path.resolve(__dirname, "src");

module.exports = {
  lintOnSave: false,

  css: {},

  configureWebpack: {
    resolve: {
      alias: {
        "@": src,
        "@assets": path.join(src, "assets"),
        "@components": path.join(src, "components"),
        "@mixins": path.join(src, "mixins"),
        "@plugins": path.join(src, "plugins"),
        "@services": path.join(src, "services"),
        "@styles": path.join(src, "styles"),
        "@store": path.join(src, "store"),
        "@router": path.join(src, "router"),
        "@typings": path.join(src, "typings"),
        "@utilities": path.join(src, "utilities"),
        "@views": path.join(src, "views"),
      },
    },
  },

  pwa: {
    name: "dirtree",
  },

  transpileDependencies: ["vuetify"],
};

/* eslint @typescript-eslint/no-var-requires: off */
