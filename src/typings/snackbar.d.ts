/** Snackbar notification */
export interface Snackbar {
  /** Snackbar button icon */
  buttonIcon?: string | null;
  /** Snackbar button text */
  buttonText?: string | null;
  /** Whether snackbar can be closed */
  closeable?: boolean;
  /** Whether the snackbar is permanent */
  permanent?: boolean;
  /** Snackbar text */
  text: string;
  /** Snackbar timeout */
  timeout?: number;
  /** Snackbar type */
  type?: "success" | "info" | "error" | "warning" | null;
  /** Whether the snackbar is visible */
  visible: boolean;
  /** Snackbar button click handler */
  onClick?: () => void;
}
