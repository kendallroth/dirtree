// Utilities
import { version } from "../package.json";

/** App config */
interface Config {
  /** Whether deployed to a production environment */
  production: boolean;
  /** App version */
  version: string;
  /** Git commit hash from build */
  versionHash: string | null;
}

/** App config */
const config: Config = {
  production: process.env.NODE_ENV === "production",
  version,
  versionHash: process.env.VUE_APP_GIT_HASH ?? null,
};

export default config;
