import Vue, { VNode } from "vue";
import ShortkeyPlugin from "vue-shortkey";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/300.css";

// Components
import App from "./App.vue";

// Utilities
import SnackbarPlugin from "@plugins/snackbar";
import VuetifyPlugin from "@plugins/vuetify";
import store from "@store";
import "./registerServiceWorker";
import router from "./router";

// Styles
import "@styles/app.scss";

Vue.config.productionTip = false;

Vue.use(ShortkeyPlugin);
Vue.use(SnackbarPlugin);

new Vue({
  router,
  store,
  vuetify: VuetifyPlugin,
  render: (h): VNode => h(App),
}).$mount("#app");
